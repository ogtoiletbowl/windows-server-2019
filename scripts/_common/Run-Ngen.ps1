Write-Output "Disabiling ngen scheduled task"
$ngen = Get-ScheduledTask '.NET Framework NGEN v4.0.30319','.NET Framework NGEN v4.0.30319 64'
$ngen | Disable-ScheduledTask -ErrorAction SilentlyContinue | Out-Null

Write-Output "Running ngen.exe"
. $env:windir\microsoft.net\framework\v4.0.30319\ngen.exe update /force /queue > NUL
. $env:windir\microsoft.net\framework64\v4.0.30319\ngen.exe update /force /queue > NUL
. $env:windir\microsoft.net\framework\v4.0.30319\ngen.exe executequeueditems > NUL
. $env:windir\microsoft.net\framework64\v4.0.30319\ngen.exe executequeueditems > NUL