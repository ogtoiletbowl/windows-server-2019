# VM

variable "cpus" {
  type = number
  default = 2
}

variable "ram" {
  type = number
  default = 2048
}

variable "network" {}

variable "disk_size" {
  type = number
  default = 8192
}

variable "disk_thin_provisioned" {
  type = bool
  default = true
}

variable "guest_os_type" {}


# Build

// variable "boot_command" {}

// variable "http_proxy" {}

// variable "https_proxy" {}

// variable "no_proxy" {}

# SSH

variable "winrm_username" {}

variable "winrm_password" {
  type = string
  default = ""
  sensitive = true
}

# vCenter

variable "vcenter_address" {}

variable "vcenter_ignore_ssl" {
  type = bool
  default = true
}

variable "vcenter_user" {}

variable "vcenter_password" {
  type = string
  default = ""
  sensitive = true
}

variable "vcenter_dc" {}

variable "vcenter_host" {}

variable "vcenter_datastore" {}

variable "vcenter_folder" {}

# OS Data

variable "os_family" {}

variable "os_version" {}

variable "os_iso_path" {}
